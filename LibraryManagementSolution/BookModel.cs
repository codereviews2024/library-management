﻿using LibraryAppLibrary.Interfaces;

using System;

namespace LibraryAppLibrary.Models
{

    public class BookModel : LibraryItemModel, IRentable, IPurchasable
    {
        public BookModel() { }
        public BookModel(string title, string author, int itemId, bool available, int stock)
        {
            Title = title;
            Author = author;
            ItemID = itemId;
            Available = available;
            Stock = stock;
        }

        public override string GetDescription()
        {
            return $"Book: {Title}, Author: {Author}, Item ID: {ItemID}, Available: {Available} ";
        }
        public void Purchase()
        {
            Console.WriteLine("This book has been purchased.");
            Stock = -1;
        }

        public void Rent()
        {
            Console.WriteLine("This book has been rented. You have 30 days to return it or a fee will be charged.");
            Stock = -1;
        }

        public void ReturnToInventory()
        {
            Console.WriteLine("This book has been returned to the inventory.");
            Stock = -1;
        }
    }
}
