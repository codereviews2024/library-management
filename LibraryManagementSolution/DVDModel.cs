﻿using LibraryAppLibrary.Interfaces;
using System;

namespace LibraryAppLibrary.Models
{

    public class DVDModel : LibraryItemModel, IRentable, IPurchasable
    {
        public DVDModel() { }

        public DVDModel(string title, string author, int itemId, bool available = true, int stock = 10)
        {
            Title = title;
            Author = author;
            ItemID = itemId;
            Available = available;
            Stock = stock;
        }



        public override string GetDescription()
        {
            return $"DVD: {Title}, Author: {Author}, Item ID: {ItemID}, Available: {Available}";
        }

        public void Purchase()
        {
            Console.WriteLine("This DVD has been purchased.");
            Stock = -1;
        }

        public void Rent()
        {
            Console.WriteLine("This DVD has been rented. You have 30 days to return it or a fee will be charged.");
            Stock = -1;
        }

        public void ReturnToInventory()
        {
            Console.WriteLine("This DVD has been returned to the inventory.");
            Stock = -1;
        }
    }
}
