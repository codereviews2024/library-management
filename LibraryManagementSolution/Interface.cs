﻿namespace LibraryAppLibrary.Interfaces
{

    public interface IPurchasable
    {
        string Title { get; set; }
        bool Available { get; set; }
        void Purchase();
    }
    public interface IRentable
    {
        string Title { get; set; }
        bool Available { get; set; }
        void Rent();
    }

}
