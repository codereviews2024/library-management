﻿namespace LibraryAppLibrary.Models
{

    public class LibraryItemModel
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public int ItemID { get; set; }
        public bool Available { get; set; } = true; // partimos de la base de que estan todos disponibles
        public int Stock { get; set; }
        public virtual string GetDescription()
        {
            return $"Title: {Title}, Author: {Author}, Item ID: {ItemID}, Available: {Available} ";
        }

    }

}
