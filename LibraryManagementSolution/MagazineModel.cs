﻿using LibraryAppLibrary.Interfaces;
using System;

namespace LibraryAppLibrary.Models
{
    public class MagazineModel : LibraryItemModel, IRentable, IPurchasable
    {
        public MagazineModel() { }

        public MagazineModel(string title, string author, int itemId, bool available = true, int stock = 10)
        {
            Title = title;
            Author = author;
            ItemID = itemId;
            Available = available;
            Stock = stock;
        }


        public override string GetDescription()
        {
            return $"Book: {Title}, Author: {Author}, Item ID: {ItemID}, Available: {Available}";
        }
        public void Purchase()
        {
            Console.WriteLine("This Magazine has been purchased.");

        }

        public void Rent()
        {
            Console.WriteLine("This Magazine has been rented. You have 30 days to return it or a fee will be charged.");
            Available = false;
        }

        public void ReturnToInventory()
        {
            Console.WriteLine("This magazine has been returned to the inventory.");
            Available = true;
        }
    }
}
