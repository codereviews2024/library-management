﻿using LibraryAppLibrary.Interfaces;
using LibraryAppLibrary.Models;
using System;
using System.Collections.Generic;

namespace LibraryManagement2024
{
    internal class Program
    {
        static void Main(string[] args)
        {

            WelcomeMessage();

            List<LibraryItemModel> libraryInventory = new List<LibraryItemModel>();
            List<IRentable> rentables = new List<IRentable>();
            List<IPurchasable> purchasables = new List<IPurchasable>();

            libraryInventory.Add(new BookModel("Cien años de Soledad", "Gabriel Garcia Marquez", 79, available: true, 10));
            libraryInventory.Add(new DVDModel("Learn German vol. 1", "Assimil Languages", 70, available: true, 10));
            libraryInventory.Add(new MagazineModel("Vogue", "Vogue", 77, available: true, 10));

            rentables.Add(new BookModel("Cien años de Soledad", "Gabriel Garcia Marquez", 79, available: true, 10));
            rentables.Add(new DVDModel("Learn German vol. 1", "Assimil Languages", 70, available: true, 10));
            rentables.Add(new MagazineModel("Vogue", "Vogue", 77, available: true, 10));

            purchasables.Add(new BookModel("Cien años de Soledad", "Gabriel Garcia Marquez", 79, available: true, 10));
            purchasables.Add(new DVDModel("Learn German vol. 1", "Assimil Languages", 70, available: true, 10));
            purchasables.Add(new MagazineModel("Vogue", "Vogue", 77, available: true, 10));

            AppMenu(libraryInventory, rentables, purchasables);
            Goodbye();

            Console.ReadLine();
        }

        private static void AppMenu(List<LibraryItemModel> libraryInventory, List<IRentable> rentables, List<IPurchasable> purchasables)
        {
            string askIfExit;

            do
            {
                Console.WriteLine("\nType the number of the action you want to accomplish.");
                Console.WriteLine("***********************************");
                Console.WriteLine("- 1. CHECK CURRENT INVENTORY - 2. ADD ITEMS TO INVENTORY  - 3. PURCHASE AN ITEM  \n- 4. RENT AN ITEM  - 5. RETURN AN ITEM - 6. TYPE 6 TO EXIT THE APP.");
                Console.WriteLine("***********************************\n");

                bool isValidNumber = int.TryParse(Console.ReadLine(), out int output);

                if (output == 1)
                {
                    CheckCurrentInventory(libraryInventory);
                }
                else if (output == 2)
                {
                    AddItemsToInventory(rentables, purchasables, libraryInventory);
                }
                else if (output == 3)
                {
                    BuyPurchasable(purchasables);
                }

                else if (output == 4)
                {
                    CheckRentables(rentables);
                }

                else if (output == 5)
                {
                    // ReturnItem(rentables);
                }

                else if (output == 6)
                {
                    break;
                }

                else
                {
                    Console.WriteLine("That was not a valid input.");
                }

                Console.WriteLine();
                Console.WriteLine("Do you want to exit the app?");
                askIfExit = Console.ReadLine();

            } while (askIfExit.ToLower() != "yes");
        }

        private static void CheckCurrentInventory(List<LibraryItemModel> libraryInventory)
        {
            foreach (var inventoryItem in libraryInventory)
            {
                Console.WriteLine(inventoryItem.GetDescription());
            }
        }

        private static void BuyPurchasable(List<IPurchasable> purchasables)
        {
            foreach (IPurchasable item in purchasables)
            {
                Console.WriteLine($"\n{item.Title}");
                Console.WriteLine($"Available for purchase? {item.Available}\n");


                string answer;
                Console.WriteLine("Do you want to purchase this item?");
                answer = Console.ReadLine();
                if (answer.ToLower() == "yes")
                {
                    item.Purchase();
                }
                else
                {
                    continue;
                }

            }

        }

        private static void CheckRentables(List<IRentable> rentables)
        {
            foreach (IRentable item in rentables)
            {
                Console.WriteLine($"{item.Title}");
                Console.WriteLine($"Available for rent? {item.Available}");
            }

        }

        private static void AddItemsToInventory(List<IRentable> rentables, List<IPurchasable> purchasables, List<LibraryItemModel> libraryInventory)
        {
            string keepAdding;
            BookModel book = new BookModel();
            MagazineModel magazine = new MagazineModel();
            DVDModel dvd = new DVDModel();


            do
            {
                Console.WriteLine("");
                Console.WriteLine("Do you want to add an item to the inventory?");
                string answer = Console.ReadLine();


                if (answer.ToLower() == "yes")
                {
                    Console.WriteLine("Wich item you want to add? Book, DVD or Magazine? ");
                    string inventoryItemType = Console.ReadLine();
                    bool isValidNumber;

                    switch (inventoryItemType.ToLower())
                    {

                        case "book":


                            Console.WriteLine("Add Book title:");
                            book.Title = Console.ReadLine();
                            Console.WriteLine("Add book author:");
                            book.Author = Console.ReadLine();
                            Console.WriteLine("Add book ID: ");
                            isValidNumber = int.TryParse(Console.ReadLine(), out int number);
                            book.ItemID = number;
                            book.Available = true;


                            rentables.Add(book);
                            purchasables.Add(book);
                            libraryInventory.Add(book);

                            Console.Clear();

                            break;

                        case "magazine":


                            Console.WriteLine("Add magazine title:");
                            magazine.Title = Console.ReadLine();
                            Console.WriteLine("Add magazine author:");
                            magazine.Author = Console.ReadLine();
                            Console.WriteLine("Add magazine ID: ");
                            isValidNumber = int.TryParse(Console.ReadLine(), out int number2);
                            magazine.ItemID = number2;
                            magazine.Available = true;

                            book.GetDescription();

                            rentables.Add(magazine);
                            purchasables.Add(magazine);

                            Console.Clear();
                            break;

                        case "dvd":


                            Console.WriteLine("Add DVD title:");
                            dvd.Title = Console.ReadLine();
                            Console.WriteLine("Add DVD author:");
                            dvd.Author = Console.ReadLine();
                            Console.WriteLine("Add DVD ID: ");
                            isValidNumber = int.TryParse(Console.ReadLine(), out int number3);
                            dvd.ItemID = number3;
                            dvd.Available = true;

                            rentables.Add(dvd);
                            purchasables.Add(dvd);

                            Console.Clear();
                            break;
                        default:
                            Console.WriteLine("That's not a valid type.");
                            break;



                    }


                }

                else
                {
                    break;
                }




                Console.WriteLine("Do you want to keep adding items? Type Yes/No.");
                keepAdding = Console.ReadLine();

            } while (keepAdding.ToLower() == "yes");



        }

        private static void WelcomeMessage()
        {
            Console.WriteLine("Welcome to Diego's Secret Library.");
            Console.WriteLine("**********************************");
            Console.WriteLine("");


        }
        private static void Goodbye()
        {
            Console.WriteLine("This app has been entirely made by Diego.");
            Console.WriteLine("Thank you for coming.");
        }
    }
}
